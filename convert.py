# Use https://raw.githubusercontent.com/vberlier/pynbs/master/pynbs.py
from typing import *
import os
import sys
import math
import pynbs


NOTE_BLOCK_OFFSET = -33


def main():
	
	args: List[str] = sys.argv[1:]
	if len(args) < 1:
		print("convert.py <target NBS file> [unlimited octaves: boolean]")
		return
	if len(args) > 2:
		print("Unknown arguments.")
		return
	
	target: str = args[0]
	
	# Ask the user if they want unlimited octaves or not.
	# They'll need a resource pack for this.
	# And Computercraft needs something to enable unlimited octaves:
	# https://github.com/SquidDev-CC/CC-Tweaked/blob/master/src/main/java/dan200/computercraft/shared/peripheral/speaker/SpeakerPeripheral.java#L101
	unlimited_octaves: bool = False
	if len(args) > 1:
		enter = args[1]
		if enter in {"y", "ye", "yes", "1", "true", "on", "tru"}:
			unlimited_octaves = True
		elif enter in {"n", "no", "0", "false", "off", "nope"}:
			# Do nothing, it's already false.
			pass
		else:
			print(f"Unknown argument for unlimited octaves: {enter}")
			return
	
	nbs: pynbs.File = pynbs.read(target)
	
	if len(nbs.instruments) > 0:
		print(
			"This doesn't support custom instruments, only vanilla noteblocks."
		)
		return
	
	writer: List[str] = []
	writer.append(f"-- Song: {nbs.header.song_name}")
	writer.append(f"-- Author: {nbs.header.original_author}")
	writer.append(f"-- NBS Author: {nbs.header.song_author}")
	writer.append(f"-- Tempo: {nbs.header.tempo}")
	writer.append(f"-- Ticks: {nbs.header.song_length}")
	writer.append(f"-- Loop: {nbs.header.loop}")
	writer.append(
		f"-- Loop amount: {nbs.header.max_loop_count}"
		if nbs.header.max_loop_count > 0
		else "-- Loop amount: Infinite"
	)
	writer.append(f"-- Loop start: {nbs.header.loop_start}")
	#writer.append(f"-- File origin: {nbs.header.song_origin}")
	
	#writer.append("local speaker = peripheral.find(\"speaker\")")
	#writer.append("local pn = speaker.playNote")
	writer.append("local pn = peripheral.find(\"speaker\").playNote")
	writer.append("local s = os.sleep")
	
	default_instr = {
		"bass": 1,
		"snare": 3,
		"hat": 4,
		"basedrum": 2,
		"guitar": 5,
		"bell": 7,
		"flute": 6,
		"chime": 8,
		"xylophone": 9,
		"iron_xylophone": 10,
		"cow_bell": 11,
		"didgeridoo": 12,
		"bit": 13,
		"banjo": 14,
		"pling": 15,
		"harp": 0
	}
	reversed_default_instr = {v: k for k, v in default_instr.items()}
	used_vanilla_instr = set()
	# Loop through it once to find what instruments are used.
	for tick, chord in nbs:
		for note in chord:
			if note.instrument in reversed_default_instr:
				used_vanilla_instr.add(reversed_default_instr[note.instrument])
	
	# Now write vanilla instruments.
	for used_i in used_vanilla_instr:
		writer.append(f"local i_{used_i} = \"{used_i}\"")
	
	writer.append("function song()")
	
	previous_tick: float = 0
	is_first: bool = True
	for tick, chord in nbs:
		
		# Don't sleep for 0.0 seconds, waste of time.
		if is_first:
			is_first = False
		else:
			# It only cares about the 2nd decimal, otherwise it rounds.
			seconds = round(tick / nbs.header.tempo - previous_tick, 2)
			
			writer.append(f"\ts({seconds})")
		previous_tick = tick / nbs.header.tempo
		
		for note in chord:
			# Did you know? You can't play two sounds at once with playSound, but you can with playNote!
			
			mc_key = note.key
			if not unlimited_octaves:
				# Should be okay.
				mc_key += NOTE_BLOCK_OFFSET
			
			#writer.append(f"\tpn(\"harp\", 1.0, {mc_key})")
			writer.append(f"\tpn(i_{reversed_default_instr[note.instrument]}, 1.0, {mc_key})")
	
	# Sleep a bit at the end?
	
	writer.append("end")
	
	
	writer.append("function main()")
	if not nbs.header.loop:
		# No looping.
		writer.append("\tsong()")
	else:
		# We do need to loop.
		# Infinitely or just a few number of times?
		# Doesn't support loop_start; TODO: maybe split it into 2 functions,
		# "intro" and "song"
		if nbs.header.max_loop_count == 0:
			# Infinite
			writer.append("\twhile true do song() end")
		else:
			# Only a number of times.
			writer.append("\tlocal i = 0")
			writer.append(f"\twhile i < {nbs.header.max_loop_count} do")
			writer.append("\t\tsong()")
			writer.append("\t\ti = i + 1")
			writer.append("\tend")
	
	writer.append("end")
	writer.append("main()")
	
	text = "\n".join(writer)
	print(text)
	
	with open(os.path.join(os.path.dirname(__file__), "startup.lua"), "w") as f:
		f.write(text)


if __name__ == "__main__":
	main()
